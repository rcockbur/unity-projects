﻿using UnityEngine;
using System.Collections;

public class PeasantScript : MonoBehaviour {

    public float moveSpeed;

    private Animator anim;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

        float inputX = Input.GetAxisRaw("Horizontal");
        float inputY = Input.GetAxisRaw("Vertical");

        anim.SetFloat("speedX", inputX);
        anim.SetFloat("speedY", inputY);

        Vector3 movement = new Vector3(
            inputX,
            inputY,
            0);

        movement.Normalize();
        movement *= (Time.deltaTime * moveSpeed);
        transform.Translate(movement);

        if (inputX != 0 || inputY != 0) {
            anim.SetBool("walking", true);
            if (inputX > 0) anim.SetFloat("lastMoveX", 1f);
            else if (inputX < 0) anim.SetFloat("lastMoveX", -1f);
            else anim.SetFloat("lastMoveX", 0);

            if (inputY > 0) anim.SetFloat("lastMoveY", 1f);
            else if (inputY < 0) anim.SetFloat("lastMoveY", -1f);
            else anim.SetFloat("lastMoveY", 0);

        } else
        {
            anim.SetBool("walking", false);
        }
    }
}
