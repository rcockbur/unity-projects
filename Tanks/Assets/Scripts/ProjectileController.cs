﻿using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour {
	
	public GameObject player;
	public float duration; //how long it will last before destroying itself

	private float startTime;

	void Start () {
		startTime = Time.time;	
	}

	void Update () {
		if ((Time.time - startTime) > duration) {
			Destroy (gameObject);
		}
	}
}
