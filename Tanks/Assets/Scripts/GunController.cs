﻿using UnityEngine;
using System.Collections;

public class GunController : MonoBehaviour {

	public float speed;

	private GameObject prefab;

	void Start () {
		prefab = Resources.Load ("Projectile") as GameObject;
	}
	
	//Respond to the return key by firing a projectile from the gun
	void Update () {
		if (Input.GetKeyDown ("return")) {
			GameObject projectile = Instantiate (prefab) as GameObject; //create projectile
			projectile.transform.position = transform.position + transform.forward * 2; //position it in front of gun
			Rigidbody rb = projectile.GetComponent<Rigidbody> ();
			rb.velocity = transform.forward * speed; //set its velocity
		}
	}
}
