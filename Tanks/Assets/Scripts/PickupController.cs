﻿using UnityEngine;
using System.Collections;

public class PickupController : MonoBehaviour {

	//rotate smoothly
	void Update () {
		transform.Rotate (new Vector3 (30, 30, -30) * Time.deltaTime);
	}

	//respond to collisions with the player or a projectile
	void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.CompareTag("Pickup"))
		{
			GameManager.instance.IncrementScore (); //increase score by 1
			Destroy(gameObject);					//delete self
		}
	}

}
