﻿using UnityEngine;
using System.Collections;

public class TurretPivotController : MonoBehaviour {

	public float sensitivity;

	private Quaternion originalRotation;
	private float rotation;

	void Start() {
		originalRotation = transform.localRotation;
		rotation = 0;
	}

	void FixedUpdate () {
		float inputVertical = Input.GetAxisRaw ("HorizontalAim");
		rotation += inputVertical * sensitivity;
		rotation = Mathf.Clamp (rotation, -30, 30);
		Quaternion newRotation = Quaternion.AngleAxis (rotation, Vector3.up);
		transform.localRotation = originalRotation * newRotation;
	}
}
