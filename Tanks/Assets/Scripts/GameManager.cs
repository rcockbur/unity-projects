﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public static GameManager instance;
	public Text countText;
	public Text winText;

	private int score;

	//setup references immediately
	void Awake(){
		instance = this;
	}

	//initialize game variables
	void Start(){
		score = 0;
		winText.text = "";
	}
		
	public void IncrementScore(){
		++score;
	}
		
	void Update(){
		countText.text = "Count: " + score.ToString ();
		if (score == 24) {
			winText.text = "You Win!";
		}
	}
		
}
