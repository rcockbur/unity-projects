﻿using UnityEngine;
using System.Collections;

public class GunPivotController : MonoBehaviour {

	public float sensitivity;

	private Quaternion originalRotation;
	private float rotation;

	void Start() {
		originalRotation = transform.localRotation;
		rotation = 0;
	}

	void FixedUpdate () {
		float inputVertical = Input.GetAxisRaw ("VerticalAim");
		rotation += inputVertical * sensitivity;
		rotation = Mathf.Clamp (rotation, 0, 45);
		Quaternion newRotation = Quaternion.AngleAxis (rotation, Vector3.left);
		transform.localRotation = originalRotation * newRotation;
	}
}
