﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	public float acceleration;
	public float maxSpeed;
	public float jumpSpeed;
	public float turnSpeed;
	public float slowDown;

	private Rigidbody rb;
	private Collider col;
	private float distToGround;

	void Start()
	{
		rb = GetComponent<Rigidbody>();
		col = GetComponent<Collider>();
		distToGround = col.bounds.extents.y; //used to check if the player is grounded
	}

	//handle movement input (walking, paning, and jumping)
	void FixedUpdate()
	{
		//handle panning
		float inputHorizontal = Input.GetAxisRaw ("HorizontalMove"); 
		rb.transform.Rotate (Vector3.up * inputHorizontal * turnSpeed);

		//handle walking and jumping if player is on the ground
		if (IsGrounded ()) {
			float inputVertical = Input.GetAxisRaw ("VerticalMove");
			Vector3 moveVertical = transform.forward * inputVertical; //calc movement vector

			//if the player is walking, apply force. if not, slow down
			if (moveVertical != Vector3.zero) rb.AddForce (moveVertical * acceleration);
			else ApplySlowdown ();

			//apply jump force
			if (Input.GetKeyDown ("space")) rb.AddForce (Vector3.up * jumpSpeed);
		}
			
		//make sure planar movement (along x,z plane) does not exceed maxSpeed
		Vector2 planarVelocity = new Vector2 (rb.velocity.x, rb.velocity.z);
		if (planarVelocity.magnitude > maxSpeed) {
			planarVelocity = planarVelocity.normalized * maxSpeed;
			rb.velocity = new Vector3 (planarVelocity.x, rb.velocity.y, planarVelocity.y);
		}
	}

	//requirement:the player is on the ground. this is innapropriate for when the player is falling
	//slows the players speed by a certain increment. If speed is less than the increment, stop.
	void ApplySlowdown(){
		if (rb.velocity.magnitude > slowDown) {
			float speedFraction = (rb.velocity.magnitude - slowDown) / rb.velocity.magnitude;
			rb.velocity = rb.velocity * speedFraction;
		} else {
			rb.velocity = Vector3.zero;
		}
	}

	//Check if the player is standing on anything.
	bool IsGrounded(){
		return Physics.Raycast(transform.position, Vector3.down, distToGround + 0.1f);
	}
}
